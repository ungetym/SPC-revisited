# SPC-revisited

Supplementary material for

**[Tim Michels](https://www.mip.informatik.uni-kiel.de/en/team/tim-michels-m-sc), Daniel Mäckelmann, [Reinhard Koch](https://www.mip.informatik.uni-kiel.de/en/team/prof.-dr.-ing.-reinhard-koch) - "Mind the Exit Pupil Gap: Revisiting the Intrinsics of a Standard Plenoptic Camera"**, *submitted to MDPI Sensors 2024*. [Preprint](https://arxiv.org/abs/2402.12891)

This repository contains the 10 SPC setups and the resulting measurements from the evaluation section of that paper.

### Contents

`Setups` contains the .blend files (check out [our Blender add-on](https://gitlab.com/ungetym/blender-camera-generator) for camera simulation) for each setup as well as the basic configuration properties in the associated `.json` files. Each of the .blend files has an associated renderings script that should automatically be shown in the Text Editor area at the bottom when opened with Blender. Executing that script (after modifying the output path to your desired location) will produce the renderings used for the evaluation measurements.

These renderings were not included here due to the large size. However, if you want to reproduce our results without re-rendering the images, feel free to contact me, so I can provide a download link!

`Scripts` contains additional python scripts used for the experiments:

`Scripts/render_mic.py` is another rendering script for Blender which was used to verify the exit pupil as origin of the microlens image centers. This script places a virtual camera at the main lens aperture center looking at the sensor direction and renders a 'positional' image, i.e. the the sensor plane material is replaced by a material emitting the hit-positions. The resulting rendering is stored as `mic.exr` and represents the first step of experiment I.

`Scripts/verify_XP.m`is used for the second step of experiment I, traces the rays from the sensor through the MLA centers into the camera and calculates the optical axis intersection as well as the blur spot sizes.

`Scripts/refocus.py` contains the refocusing and error calculation operations for the experiments II - IV. Check the documentation within that file for further specifics.

`Scripts/verify_pertuz_correction.m` calculates the data for experiment V., i.e. the correction of the refocusing model presented by Pertuz et al.. 

`Results` contains the measurements as well as ground truth data, i.e. directly calculated values, for the various experiments performed via the scripts listed above. The subfolders are named according to the respective experiments and contain a description of the file parameters. 



### Contact

For further help, notes or requests feel free to contact me via [tim.michels@posteo.de](mailto:tim.michels@posteo.de) 

*_blur_spots.csv contains data about the blur spot sizes close to the exit pupil. The columns describe:
Signed distance from H_cam, blur spot size for 100% of rays, blur spot size for 75% of rays, blur spot size for 50% of rays, blur spot size for 25% of rays

*_mic_origin.csv contains the intersections of back-traced rays with the optical axis in terms of signed distance to H_cam.

*_mic_origin_meanvar.csv contains the mean and variance of the intersections. The values describe:
Camera prefix, mean, variance, X in mm
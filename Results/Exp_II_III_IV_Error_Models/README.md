The files *_error_o_incorrect_s.csv contain the data for an incorrectly (assumption X=0) calculated object distance combined with a correct shift. The columns are:
o_GT			Ground truth object distance
Error_GT		Directly calculated error according to our model
Error_measured		Measured error
lambda			Relative object distance as in the paper
o_predicted		Predicted, i.e. directly calculated object distance based on the correct shift value and X=0
o_measured		Measured object distance
s_GT			Ground truth shift
s_measured		Measured shift

The files *_error_o_s_incorrect.csv contain the data for a correct object distance combined with an incorrect (assumption X=0)  shift. The columns are:
o_GT			Ground truth object distance
Error_GT		Directly calculated error according to our model
Error_measured		Measured error
lambda			Relative object distance as in the paper
o_predicted		Predicted, i.e. directly calculated object distance based on the incorrect shift value
o_measured		Measured object distance
s_incorrect		Calculated, incorrect shift
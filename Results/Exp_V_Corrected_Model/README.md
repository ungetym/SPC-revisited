The estimated_params.csv contains the following columns:

Setup			- Name of the camera setup
a012,a112,RMSE12	- a0 and a1 as well as RMSE according to Pertuz et al.
a0our,a1our,RMSEour	- a0, a1 and RMSE directly calculated via our correction
a0,a1,RMSE		- a0, a1 and RMSE fitted to the data of experiment II
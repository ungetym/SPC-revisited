clear;
close all;

%% Config
plot_compare_absolute = 1;
plot_compare_rel = 1;
plot_error_margin_abs = 1;
plot_error_margin_rel = 1;

plot_s_incorrect_abs = 1;
plot_s_incorrect_rel = 1;

margin_based_on_disp_accuracy = true;
disp_accuracy = 0.02; %in px

%% Camera data
prefixes = {'4_', '3_', '2_', '1_', '0_', '5_', '6_', '7_', '8_', '9_'};
dirs = {'Olympus', 'Canon', 'Ricoh', 'Zeiss', 'Rodenstock', 'Rodenstock_inf', 'Zeiss_inf', 'Ricoh_inf', 'Canon_inf', 'Olympus_inf'}; % Manufacturer
fs = [85.11955853470, 84.99767307639183, 167.99385034462142, 82.04737667260571, 99.998168945, 99.998168945, 82.86004638671875, 173.11544799804688, 84.99766540527344, 85.0042495727539]; % Main lens focal lengths
fMLs = [0.88453698158+0.01, 1.7785600423812866+0.01, 3.2605555057525635+0.01, 2.084439277648926+0.01, 1.290186882+0.01, 1.031753420829773+0.01, 0.9978286027908325+0.01, 1.2300399541854858+0.01, 1.383607029914856+0.01, 1.2971161603927612+0.01]; % Microlens focal lengths
Xs = [-60.2187, -28.93825874, 99.908426403, 40.651839183, 0.1944, 0.1944, 39.572854996, 91.854137421, -28.938364744, -60.308034897]; % Exit pupil to H_cam distances
pxs = [7.222219944000244 / 585, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492/1161, 23.200000762939453/1161, 23.219999313354492/1161, 22.31999969482422/1161];
o_mms = [300, 300, 500, 500, 500, 60000, 250000, 1000000, 1000000, 60000]; % Focus distance of MLA, i.e. zero shift plane
ranges_mm = {100:599;100:599;200:799;200:799;200:799;150:10:2999;150:10:2999;150:10:2999;150:10:2999;150:10:2999}; % Range of focus distances
pitchs = [110.567001342, 177.8560028076172, 176.24624633789062, 173.7030029296875, 178.15823364257812, 178.15823364257812, 175.9448699951172, 177.16424560546875, 177.84036254882812, 171.49252319335938];
%d_mms = [-1; 117.39794921875-1.0-0.1--2.3025809234];

results = [];

for cam = 1:10
    
    f_mm = fs(cam);
    f_ML_mm = fMLs(cam);
    X_mm = Xs(cam);
    px_mm = pxs(cam);
    o_mm = o_mms(cam);
    range_mm = ranges_mm{cam};
    pitch_um = pitchs(cam);

    % Load measured data
    T = readtable(strcat("YOURPATHTOTHECAMERADIRS",prefixes{cam},dirs{cam},"/",prefixes{cam},"error_o_incorrect_s.csv"));

    % Calculate further values
    d_mm = 1/(1/f_mm-1/o_mm);
    delta_ST = pitch_um*0.001;
    delta_UV = px_mm * (d_mm - X_mm) / f_ML_mm;
    delta = delta_UV/delta_ST;
    delta_wrong = delta * d_mm / (d_mm - X_mm);
    
    % Calculate a_0 and a_1 according to Pertuz et al. and according to our
    % formula
    a_0_pertuz = f_ML_mm * pitch_um/1000 / (px_mm * d_mm);
    a_1_pertuz = o_mm * a_0_pertuz / f_mm;

    a_0_our = -X_mm / (delta * d_mm);
    a_1_our = -(X_mm - f_mm) / (delta*(d_mm - f_mm));

    a_0_max_predict = max(abs(a_0_pertuz),abs(a_0_our));
    global_a_0_range = linspace(-a_0_max_predict,a_0_max_predict,10);
    a_1_max_predict = max(abs(a_1_pertuz),abs(a_1_our));
    global_a_1_range = linspace(-a_1_max_predict,a_1_max_predict,10);

    % Model to fit in order to find measured a_0 and a_1
    model = @(a,s) o_mm * (1-a(1)*s) ./ (1-a(2)*s);
    %opts = statset('TolFun',1e-10);

    % Grid search
    best_params = fitnlm(-T.s_measured, T.o_GT, model,[0,0]);%,'Options',opts);
    for a_0_init = global_a_0_range
        for a_1_init = global_a_1_range
            params = fitnlm(-T.s_measured, T.o_GT, model,[a_0_init,a_1_init]);%,'Options',opts);
            if params.RMSE < best_params.RMSE
                best_params = params;
            end
        end
    end
    

    rmse_pertuz = (model([a_0_pertuz,a_1_pertuz], -T.s_measured) - T.o_GT);
    rmse_pertuz = sqrt(1/(size(T,1)-2)  * rmse_pertuz'*rmse_pertuz);

    rmse_our = (model([a_0_our,a_1_our], -T.s_measured) - T.o_GT);
    rmse_our = sqrt(1/(size(T,1)-2)  * rmse_our'*rmse_our);

    results = [results; a_0_pertuz a_1_pertuz rmse_pertuz a_0_our a_1_our rmse_our best_params.Coefficients.Estimate(1) best_params.Coefficients.Estimate(2) best_params.RMSE];

end

csvwrite("Data_out/verify_model.csv", results);
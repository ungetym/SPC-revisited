clear

%% Camera data
prefixes = {'4_', '3_', '2_', '1_', '0_', '5_', '6_', '7_', '8_', '9_'};
dirs = {'Olympus', 'Canon', 'Ricoh', 'Zeiss', 'Rodenstock', 'Rodenstock_inf', 'Zeiss_inf', 'Ricoh_inf', 'Canon_inf', 'Olympus_inf'}; % Manufacturer
fs = [85.11955853470, 84.99767307639183, 167.99385034462142, 82.04737667260571, 99.998168945, 99.998168945, 82.86004638671875, 173.11544799804688, 84.99766540527344, 85.0042495727539]; % Main lens focal lengths
fMLs = [0.88453698158+0.01, 1.7785600423812866+0.01, 3.2605555057525635+0.01, 2.084439277648926+0.01, 1.290186882+0.01, 1.031753420829773+0.01, 0.9978286027908325+0.01, 1.2300399541854858+0.01, 1.383607029914856+0.01, 1.2971161603927612+0.01]; % Microlens focal lengths
Xs = [-60.2187, -28.93825874, 99.908426403, 40.651839183, 0.1944, 0.1944, 39.572854996, 91.854137421, -28.938364744, -60.308034897]; % Exit pupil to H_cam distances
pxs = [7.222219944000244 / 585, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492/1161, 23.200000762939453/1161, 23.219999313354492/1161, 22.31999969482422/1161];
o_mms = [300, 300, 500, 500, 500, 60000, 250000, 1000000, 1000000, 60000]; % Focus distance of MLA, i.e. zero shift plane
ranges_mm = {100:599;100:599;200:799;200:799;200:799;150:10:2999;150:10:2999;150:10:2999;150:10:2999;150:10:2999}; % Range of focus distances
pitchs = [110.567001342, 177.8560028076172, 176.24624633789062, 173.7030029296875, 178.15823364257812, 178.15823364257812, 175.9448699951172, 177.16424560546875, 177.84036254882812, 171.49252319335938];

result_mean_error = [];
to_print = [];

figure(1);
hold on;

% Current cam index
for cam = 1:10

% Get current values
f_mm = fs(cam);
f_ML_mm = fMLs(cam)-0.01;
X_mm = Xs(cam);
px_mm = pxs(cam);
o_mm = o_mms(cam);
range_mm = ranges_mm{cam};
pitch_um = pitchs(cam);
threshold = pitch_um/1000; %um to mm

dist_H_cam_MLA = 1/(1/f_mm - 1/o_mm);
dist_exit_mla = dist_H_cam_MLA - X_mm;
magnif_predict = (dist_exit_mla + f_ML_mm)/dist_exit_mla;

% Load MIC image
[I, ~] = exrread(strcat("YOURPATHTOTHECAMERADIRS",prefixes{cam},dirs{cam},"/mic.exr"));
[w,h,c] = size(I);

% Separate the channels
red_channel = I(:,:,1)*1000; %m to mm
green_channel = I(:,:,2)*1000; %m to mm
blue_channel = I(:,:,3);

% Define a 3x3 neighborhood kernel
kernel = [0 0.2 0; 0.2 0.2 0.2; 0 0.2 0];

% Compute local standard deviation for red and green channels
avg_red = abs(conv2(red_channel,kernel,"same") - red_channel);
avg_green = abs(conv2(green_channel,kernel,"same") - green_channel);

% Identify pixels for potential clustering
cluster_mask = (blue_channel == 1.0) & ...
               (avg_red <= 0.2*threshold) & ...
               (avg_green <= 0.2*threshold) & ...
               abs(red_channel - green_channel) <= 0.1*threshold; % Only main diagonal of mls
                
% Initialize cluster mean array
cluster_means = [];

% Label the clusters
[labeled_clusters, num_clusters] = bwlabel(cluster_mask, 8);

% Calculate mean for each cluster
for i = 1:num_clusters
    % Get the indices of the pixels in the current cluster
    [rows, cols] = find(labeled_clusters == i);

    % Extract (r, g, b) values for these pixels
    cluster_pixels = zeros(length(rows), 2);
    for j = 1:length(rows)
        cluster_pixels(j, :) = [red_channel(rows(j), cols(j)), green_channel(rows(j), cols(j))];
    end

    % Calculate the mean of (r, g, b) for the current cluster
    cluster_mean = mean(cluster_pixels, 1);
    if sqrt(cluster_mean(1)*cluster_mean(1)+cluster_mean(2)*cluster_mean(2)) > 2*threshold
        cluster_means = [cluster_means; cluster_mean, dist_H_cam_MLA + f_ML_mm];
    end
end

% Output the cluster means
if false
    figure(1);
    hold on;
    axis equal;
    scatter(cluster_means(:,1),cluster_means(:,2))
end

% Create ray bundle
directions = cluster_means / magnif_predict;
directions = round(1000 * directions / pitch_um)* pitch_um/1000;% - cluster_means;
directions(:,3) = dist_H_cam_MLA;

% Calculate optical axis intersection for every ray
rot_means = [sqrt(cluster_means(:,1).*cluster_means(:,1)+cluster_means(:,2).*cluster_means(:,2)),cluster_means(:,3)];
rot_dirs = [sqrt(directions(:,1).*directions(:,1)+directions(:,2).*directions(:,2)),directions(:,3)];
intersections = rot_means(:,2)-(rot_means(:,1))./(rot_dirs(:,1)-rot_means(:,1)).*(rot_dirs(:,2)-rot_means(:,2));

figure(1);
hold on;
scatter(cam*ones(size(intersections,1),1),intersections);
csvwrite(strcat(prefixes{cam},"mic_origin.csv"),intersections);

means = mean(intersections);
to_print=[to_print,means-X_mm];
vars = var(intersections);
result_mean_error = [str2num(prefixes{cam}(1:end-1)), means, vars, X_mm];
csvwrite(strcat(prefixes{cam},"mic_origin_meanvar.csv"), result_mean_error);



% Calculate blur spot sizes around exit pupil location
directions = directions - cluster_means;
directions(:,3) = dist_H_cam_MLA-0.1;
blur_spots = [];
pos_step=0.05;
current_pos = X_mm - 10;
num_points = size(cluster_means,1);

current_points = cluster_means + (dist_H_cam_MLA + f_ML_mm - current_pos)/f_ML_mm * directions;
rays = [cluster_means,current_points];

while current_pos < X_mm + 10
    current_points = cluster_means + (dist_H_cam_MLA + f_ML_mm - current_pos)/f_ML_mm * directions;
    current_blur_all = max(sqrt(current_points(:,1).^2+current_points(:,2).^2));
    current_blur_75 = max(sqrt(current_points(ceil(0.125*num_points):ceil(0.875*num_points),1).^2+current_points(ceil(0.125*num_points):ceil(0.875*num_points),2).^2));
    current_blur_50 = max(sqrt(current_points(ceil(0.25*num_points):ceil(0.75*num_points),1).^2+current_points(ceil(0.25*num_points):ceil(0.75*num_points),2).^2));
    current_blur_25 = max(sqrt(current_points(ceil(0.375*num_points):ceil(0.625*num_points),1).^2+current_points(ceil(0.375*num_points):ceil(0.625*num_points),2).^2));
    blur_spots = [blur_spots; current_pos, current_blur_all, current_blur_75, current_blur_50, current_blur_25];
    current_pos = current_pos + pos_step;
end
csvwrite(strcat(prefixes{cam},"blur_spots.csv"), blur_spots);
csvwrite(strcat(prefixes{cam},"rays.csv"), rays);

figure(4);
hold on;
plot(blur_spots(:,1),blur_spots(:,2));
plot(blur_spots(:,1),blur_spots(:,3));
plot(blur_spots(:,1),blur_spots(:,4));
plot(blur_spots(:,1),blur_spots(:,5));
end
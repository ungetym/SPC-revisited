import os
os.environ["OPENCV_IO_ENABLE_OPENEXR"]="1"
import csv
import cupy
import cupyx.scipy.ndimage as ndimage_gpu
import cv2
from datetime import datetime
from joblib import Parallel, delayed
import numpy as np


def demultiplex(devignetted_image_gpu, lens_size=9):
    """Creates an array of sub-aperture images from a raw SPC image based on the assumption of a regular grid of microlens images of the integer size lens_size X lens_size

    Args:
        devignetted_image_gpu (_type_): Devignetted raw image as cupy array 
        lens_size (int, optional): Microlens image edge size. Defaults to 9.

    Returns:
        _type_: cupy array containing lens_size X lens_size sub-aperture images
    """
    # Dimensions of the sub-aperture image
    height, width = devignetted_image_gpu.shape[0] // lens_size, devignetted_image_gpu.shape[1] // lens_size

    # Initialize the sub-aperture images
    sub_images_gpu = cupy.zeros((lens_size, lens_size, height, width), dtype=np.float32)

    # Coordinates for the sub-aperture image locations
    sub_image_x = cupy.arange(devignetted_image_gpu.shape[0]) % lens_size
    sub_image_y = cupy.arange(devignetted_image_gpu.shape[1]) % lens_size
    pixel_x = cupy.arange(devignetted_image_gpu.shape[0]) // lens_size
    pixel_y = cupy.arange(devignetted_image_gpu.shape[1]) // lens_size

    # Use CuPy's advanced indexing to demultiplex
    sub_images_gpu = cupy.zeros((lens_size, lens_size, height, width), dtype=cupy.float32)
    sub_images_gpu[sub_image_x[:, None], sub_image_y, pixel_x[:, None], pixel_y] = devignetted_image_gpu

    return sub_images_gpu

def refocus(sub_images_gpu, s: float):
    """Shift-and-sum refocus algorithm for a stack of sub-aperture images

    Args:
        sub_images_gpu (_type_): Sub-aperture images as cupy array
        s (float): Shift amount between neighboring images in px

    Returns:
        _type_: Refocused image as cupy array
    """
    # Dimensions of the sub-images
    lens_size, _, height, width = sub_images_gpu.shape

    # Initialize the refocused image (with same size and number of channels as a sub-image)
    refocused_image_gpu = cupy.zeros((height, width), dtype=cupy.float32)

    # Apply the shift and sum the sub-images
    for i in range(1,lens_size-1):
        for j in range(1,lens_size-1):

            if (i==1 or i==lens_size-2) and (j ==1 or j==lens_size-2):
                continue

            shift_x = (i - lens_size // 2) * s
            shift_y = (j - lens_size // 2) * s
            shifted_image_gpu = ndimage_gpu.shift(sub_images_gpu[i, j], (shift_x, shift_y), order=3, mode='nearest')
            refocused_image_gpu += shifted_image_gpu

    # Normalize
    refocused_image_gpu /= ((lens_size-2) * (lens_size-2) - 4)

    return refocused_image_gpu

def calc_sharpness(refocused_image_gpu):
    """Image sharpness measurement based on the varaince of the Laplacian

    Args:
        refocused_image_gpu (_type_): Refocused image as cupy array

    Returns:
        _type_: Sharpness value
    """
    # Dimensions of the sub-images
    height, width = refocused_image_gpu.shape  
    # Extract the center region
    center_coord_x = int((width-1)/2)
    center_coord_y = int((height-1)/2)
    hws_x = int((width-1)/4)
    hws_y = int((height-1)/4)
    center_region = refocused_image_gpu[center_coord_y-hws_y:center_coord_y+hws_y, center_coord_x-hws_x:center_coord_x+hws_x]
    
    # Apply the Laplacian filter
    dx = ndimage_gpu.sobel(center_region, 0)  # Horizontal derivative
    dy = ndimage_gpu.sobel(center_region, 1)  # Vertical derivative
    laplacian_gpu = dx**2 + dy**2

    # Calculate the variance of the Laplacian
    sharpness = float(cupy.var(laplacian_gpu))

    return sharpness

def process_file(raw_array, white_array_gpu, f_mm, d_mm, X_mm, delta, delta_wrong, o_mm, S_incorrect, dist_mm, dist_range):
    """_summary_

    Args:
        raw_array (_type_): Raw SPC image as numpy array
        white_array_gpu (_type_): White image for devignetting as cupy array
        f_mm (_type_): Main lens focal length in mm
        d_mm (_type_): Distance between H_cam and MLA in mm
        X_mm (_type_): Signed distance between H_cam and the exit pupil in mm
        delta (_type_): Rescaling parameter compensating for different sample frequencies in UV and ST plane
        delta_wrong (_type_): Like delta, but calculated under the assumption X_mm = 0
        o_mm (_type_): Refocus distance in mm
        S_incorrect (_type_): Shift calculated under the assumption X_mm = 0
        dist_mm (_type_): Ground truth target distance
        dist_range (_type_): Range of rendered object distances

    Returns:
        _type_: Various error and the corresponding ground truth values
    """
    # Theoretical shift
    o_GT = dist_mm
    S_o = delta * (o_GT * (f_mm - d_mm) + f_mm * d_mm) / (o_GT * (f_mm - X_mm) + f_mm * X_mm)

    # Transfer data to gpu
    raw_array_gpu = cupy.array(raw_array)

    # Devignetting
    mask_zero = white_array_gpu == 0
    raw_array_gpu[~mask_zero] /= white_array_gpu[~mask_zero]
    raw_array_gpu[mask_zero] = 0
    #print('devignetting done')

    # Apply demultiplexing
    sub_images_gpu = demultiplex(raw_array_gpu)
    #print('demultiplexing done')

    # Apply refocusing for the range of values and calculate sharpness
    max_sharpness = 0
    result_o_incorrect_s = []


    best_shift = S_o
    hws = 0.1 * S_o

    for run in range(0,4):

        # Range of shift values for refocusing
        shift_values = np.linspace(best_shift-hws, best_shift+hws, 11) 
        for s in shift_values:
            refocused_image_gpu = refocus(sub_images_gpu, s)
            sharpness = calc_sharpness(refocused_image_gpu)

            if sharpness > max_sharpness:
                max_sharpness = sharpness
                o_predicted = f_mm*d_mm*delta_wrong / (S_o*f_mm - delta_wrong*(f_mm-d_mm))
                error_GT = (o_GT - o_predicted )/o_GT
                o_measured = f_mm*d_mm*delta_wrong / (s*f_mm - delta_wrong*(f_mm-d_mm))
                error_measured = (o_GT - o_measured)/o_GT
                if o_mm == 0:
                    lambda_ = 0
                else:
                    lambda_ = o_GT / o_mm
                result_o_incorrect_s = [s, dist_mm, sharpness, error_measured, error_GT, lambda_, o_predicted, o_measured, S_o]

        best_shift = result_o_incorrect_s[0]
        hws = (4*hws) / (11-1)


    print(f"{o_GT} mm: Predicted: {result_o_incorrect_s[6]} Measured: {result_o_incorrect_s[7]} Shift_p: {S_o} Shift_m: {result_o_incorrect_s[0]}")

    # Apply refocusing for the range of incorrect shifts
    result_o_s_incorrect = []
    o_GT = min(dist_range)
    o_GT_step = dist_range[1]-dist_range[0]
    max_sharpness = 0
    for s in S_incorrect:
        
        error_measured = (o_GT - dist_mm)/o_GT
        o_predicted = f_mm*(d_mm*delta - s*X_mm)/(s*(f_mm-X_mm)-delta*(f_mm-d_mm))

        if abs(o_predicted - dist_mm) > 10 * o_GT_step:
            o_GT += o_GT_step
            continue

        error_GT = (o_GT - o_predicted)/o_GT
        if o_mm == 0:
            lambda_ = 0
        else:
            lambda_ = o_GT / o_mm

        refocused_image_gpu = refocus(sub_images_gpu, s)

        sharpness = calc_sharpness(refocused_image_gpu)
        max_sharpness = max(max_sharpness, sharpness)

        result_o_s_incorrect.append([s, o_GT, sharpness, error_measured, error_GT, lambda_, o_predicted, dist_mm])

        o_GT += o_GT_step
    
    for result in result_o_s_incorrect:
        result[2] /= max_sharpness
    
    result_o_s_incorrect

    return [result_o_incorrect_s, result_o_s_incorrect]

def load_image(dist_mm: float, directory_path: str):
    """Loads an SPC rendering

    Args:
        dist_mm (float): Object distance as used in the filename
        directory_path (str): File directory

    Returns:
        _type_: Image as numpy array
    """
    filename = f"{dist_mm}.exr"
    filepath = os.path.join(directory_path, filename)
    
    # Open the raw image
    raw_image = cv2.imread(filepath, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH | cv2.IMREAD_UNCHANGED)
    raw_array = np.array(raw_image)
    return raw_array[:,:,0]

def process_cam(prefix, directory_path, f_mm, f_ML_mm, X_mm, px_mm, o_mm, pitch_um, dist_range):  
    """Organizes all operations for a camera, which includes image loading, error calculations and combining the results from different calculations

    Args:
        prefix (_type_): Camera prefix like 0_, 1_ etc.
        directory_path (_type_): SPC renderings path
        f_mm (_type_): Main lens focal length in mm
        f_ML_mm (_type_): Microlens focal length in mm
        X_mm (_type_): Signed distance between H_cam and the exit pupil in mm
        px_mm (_type_): Pixel size in mm
        o_mm (_type_): Focus distance in mm
        pitch_um (_type_): Microlens pitch in um
        dist_range (_type_): Range of rendered target distances for this camera
    """

    # Calculate distance between H_cam and MLA
    if o_mm == 0:
        d_mm = f_mm
    else:
        d_mm = 1/(1/f_mm-1/o_mm)
    # Calculate scalings between ST and UV plane
    delta_ST = pitch_um*0.001
    delta_UV = px_mm * (d_mm - X_mm) / f_ML_mm
    delta = delta_UV/delta_ST
    delta_wrong = delta * d_mm / (d_mm - X_mm) 

    # Calculate object distance error based on incorrect shift
    S_incorrect = []
    for dist_mm in dist_range:
        S_incorrect.append(delta_wrong * (dist_mm * (f_mm - d_mm) + f_mm * d_mm) / (dist_mm * f_mm))

    # Load white image
    print(datetime.now())
    print('Loading white image..')
    white_image = cv2.imread(os.path.join(directory_path, 'white.exr'), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH | cv2.IMREAD_UNCHANGED)
    white_array = np.array(white_image)
    white_array = white_array[:,:,0]
    white_array_gpu = cupy.array(white_array)
    print(datetime.now())
    print('..done.')

    # Load all images to RAM - since I had to load the images repeatedly during debugging, the array containing all images is stored as binary file after the first run, so it can be loaded faster in the following runs. Uncomment this for debugging and indent the else case lines.
    print(datetime.now())
    print("Start image loading..")
    #filepath = os.path.join(directory_path, 'preloaded.npy')
    #if os.path.exists(filepath):
    #    raw_arrays = np.load(filepath)
    #    print(datetime.now())
    #    print("..done.")
    #else:
    raw_arrays = Parallel(n_jobs=16)(delayed(load_image)(dist_mm, directory_path) for dist_mm in dist_range)         
    raw_arrays = np.array(raw_arrays)   
    print(datetime.now())
    print("..done.")
    #    np.save(filepath, raw_arrays)
        

    # Loop over the specified files
    print(datetime.now())
    print("Start processing..")

    results = Parallel(n_jobs=2)(delayed(process_file)(raw_arrays[counter,:,:], white_array_gpu, f_mm, d_mm, X_mm, delta, delta_wrong, o_mm, S_incorrect, dist_range[counter], dist_range) for counter in range(0, len(dist_range)))
    # results = []
    # counter = 0
    # for dist_mm in dist_range:
    #     results.append(process_file(raw_arrays[counter,:,:], white_array_gpu, f_mm, d_mm, X_mm, delta, delta_wrong, o_mm, S_incorrect, dist_mm, dist_range))
    #     counter += 1
    print(datetime.now())
    print("..done.")

    # Combine the results
    print(datetime.now())
    print("Store results..")

    error_o_incorrect_s = {}
    error_o_s_incorrect = {}
    error_o_s_incorrect_collect = {}
    for result in results:
        key = f"{result[0][1]}"
        error_o_incorrect_s[key] = result[0]

        for result_2 in result[1]:
            key = f"{result_2[0]}"
            if key in error_o_s_incorrect_collect:
                error_o_s_incorrect_collect[key].append(result_2)
            else:
                error_o_s_incorrect_collect[key] = [result_2]
    
    for key, results_for_inc_s in error_o_s_incorrect_collect.items():
        relative_sharpness = []
        target_dist = []
        for result in results_for_inc_s:
            relative_sharpness.append(result[2])
            target_dist.append(result[7])

        min_idx = np.argmin(target_dist)
        max_idx = np.argmax(target_dist)
        if relative_sharpness[min_idx] == 1 or relative_sharpness[max_idx] == 1:
            continue

        target_dist_filtered = [dist for s, dist in zip(relative_sharpness, target_dist) if s > 0.5]
        relative_sharpness_filtered = [s for s in relative_sharpness if s > 0.5]

        if len(target_dist_filtered) < 3:
            continue

        # Fit poly for interpolation
        poly = np.polyfit(target_dist_filtered, relative_sharpness_filtered, 2)
        # Get maximum position
        best_target_dist = -poly[1]/(2*poly[0])
        result_to_modify = results_for_inc_s[0]
        result_to_modify[7] = best_target_dist
        # Calculate sharpness
        result_to_modify[2] = np.polyval(poly, best_target_dist)
        # Caluclate measured error
        result_to_modify[3] = (result_to_modify[1] - best_target_dist)/result_to_modify[1]
        # Store result
        key_store = f"{result_to_modify[1]}"
        error_o_s_incorrect[key_store] = result_to_modify


    # Paths to the CSV files
    error_o_incorrect_s_csv_path = os.path.join(directory_path, prefix+'error_o_incorrect_s.csv')
    error_o_s_incorrect_csv_path = os.path.join(directory_path, prefix+'error_o_s_incorrect.csv')

    # Writing the sharpness scores to a CSV file
    with open(error_o_incorrect_s_csv_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)

        # Write the header
        writer.writerow(['o_GT', 'Error_GT', 'Error_measured', 'lambda', 'o_predicted', 'o_measured', 's_GT', 's_measured'])

        # Write the data
        for o_GT, val in error_o_incorrect_s.items():
            writer.writerow([o_GT, val[4], val[3], val[5], val[6], val[7], val[8], val[0]])

    # Writing errors to a CSV file
    with open(error_o_s_incorrect_csv_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)

        # Write the header
        writer.writerow(['o_GT', 'Error_GT', 'Error_measured', 'lambda', 'o_predicted', 'o_measured', 's_incorrect'])

        # Write the data
        for o_GT, val in error_o_s_incorrect.items():
            writer.writerow([o_GT, val[4], val[3], val[5], val[6], val[7], val[0]])

    print(datetime.now())
    print("..done.")

################################   Application  #######################################

# My file structure within the base_path dir is the following:
# For every camera there is a dir with name 'prefix+camera name', e.g. '4_Olympus', 
# indicating the respective setup from the paper. In such a camera dir, the renderings
# are stored named according to the target distance, e.g. for the first setup 100.exr, 
# 101.exr, ... ,600.exr. In addition there is a white image for devignetting called 
# white.exr. If you require exemplary data, please feel free to contact me!

# Set dirpaths
base_path = 'PATH_TO_THE_DIR_CONTAINING_THE_CAMERA_SUBDIRS'
prefixes = ['4_', '3_', '2_', '1_', '0_', '5_', '6_', '7_', '8_', '9_']
dirs = ['Olympus', 'Canon', 'Ricoh', 'Zeiss', 'Rodenstock', 'Rodenstock_inf', 'Zeiss_inf', 'Ricoh_inf', 'Canon_inf', 'Olympus_inf']
# Set camera specific data - check description of process_cam() for documentation
fs = [85.11955853470, 84.99767307639183, 167.99385034462142, 82.04737667260571, 99.998168945, 99.998168945, 82.86004638671875, 173.11544799804688, 84.99766540527344, 85.0042495727539]
fMLs = [0.88453698158+0.01, 1.7785600423812866+0.01, 3.2605555057525635+0.01, 2.084439277648926+0.01, 1.290186882+0.01, 1.031753420829773+0.01, 0.9978286027908325+0.01, 1.2300399541854858+0.01, 1.383607029914856+0.01, 1.2971161603927612+0.01]
Xs = [-60.2187, -28.93825874, 99.908426403, 40.651839183, 0.1944, 0.1944, 39.572854996, 91.854137421, -28.938364744, -60.308034897]
pxs = [7.222219944000244 / 585, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492 / 1161, 23.219999313354492/1161, 23.200000762939453/1161, 23.219999313354492/1161, 22.31999969482422/1161]
o_mms = [300, 300, 500, 500, 500, 60000, 250000, 1000000, 1000000, 60000]
pitchs = [110.567001342, 177.8560028076172, 176.24624633789062, 173.7030029296875, 178.15823364257812, 178.15823364257812, 175.9448699951172, 177.16424560546875, 177.84036254882812, 171.49252319335938]
dist_ranges = [range(100, 600, 1), range(100, 600, 1), range(200, 800, 1), range(200, 800, 1), range(200, 800, 1), range(150, 3000, 10), range(150, 3000, 10), range(150, 3000, 10), range(150, 3000, 10), range(150, 3000, 10)]

# Loop over all setups
for cam_idx in range(0,len(dirs)):

    prefix = prefixes[cam_idx]
    directory_path =  base_path + prefix + dirs[cam_idx]
    f_mm = fs[cam_idx]
    f_ML_mm = fMLs[cam_idx]
    X_mm = Xs[cam_idx]
    px_mm = pxs[cam_idx]
    o_mm = o_mms[cam_idx]
    pitch_um = pitchs[cam_idx]
    dist_range = dist_ranges[cam_idx]

    process_cam(prefix, directory_path, f_mm, f_ML_mm, X_mm, px_mm, o_mm, pitch_um, dist_range)
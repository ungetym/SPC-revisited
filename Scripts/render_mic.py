import bpy
import math
import numpy as np

# Render setup
bpy.data.scenes["Scene"].cycles.samples = 16
bpy.data.scenes["Scene"].camera_generator.prop_sensor_resolution_x = 2048
bpy.data.scenes["Scene"].camera_generator.prop_sensor_resolution_y = 2048
bpy.data.scenes["Scene"].camera_generator.prop_sensor_exposure = 1
bpy.data.scenes["Scene"].render.image_settings.file_format = 'OPEN_EXR'

# Position camera
bpy.data.objects["cg.Orthographic_Camera"].location[0] = -bpy.data.objects["cg.Sensor"].location[0]
bpy.data.objects["cg.Orthographic_Camera"].rotation_euler[2]=math.pi/2
bpy.data.cameras["cg.Orthographic_Camera"].type = 'PERSP'
bpy.data.cameras["cg.Orthographic_Camera"].lens = 10

# Get sensor material
mat = bpy.data.materials["cg.Diffusor_Material"]
nodes = mat.node_tree.nodes

# Create geometry node
geom_node = nodes.new(type='ShaderNodeNewGeometry')
# Create separation node
sep_node = nodes.new(type="ShaderNodeSeparateXYZ")
# Create combine node
comb_node = nodes.new(type="ShaderNodeCombineXYZ")
# Create emission node
emission_node = nodes.new(type='ShaderNodeEmission')
# Get material output node
output_node = nodes['Material Output']

# Connect geometry to separation
mat.node_tree.links.new(geom_node.outputs['Position'], sep_node.inputs[0])
# Connect separation to combination node
mat.node_tree.links.new(sep_node.outputs[1], comb_node.inputs[0])
mat.node_tree.links.new(sep_node.outputs[2], comb_node.inputs[1])
comb_node.inputs[2].default_value = 1.0
# Connect geometry to emission
mat.node_tree.links.new(comb_node.outputs[0], emission_node.inputs['Color'])
# Connect emission to material output
mat.node_tree.links.new(emission_node.outputs['Emission'], output_node.inputs['Surface'])

# Remove incorrect normal in MLA material if available
mat = bpy.data.materials["cg.MLA_Rect_Material"]
nodes = mat.node_tree.nodes
links = mat.node_tree.links
refraction_node = nodes.get("ML Back Shader")
for link in links:
    if link.to_socket == refraction_node.inputs[3]:
        links.remove(link)
        
# Render image and analyze non-zero section
bpy.context.scene.render.filepath = 'mic_test.exr'
bpy.ops.render.render(write_still = True)
# Load rendered image to array
image = bpy.data.images.load(bpy.context.scene.render.filepath)
data_array = np.asarray(image.pixels)
values = data_array[1023*2048*4:1024*2048*4:4]
# Remove image
#remove(bpy.context.scene.render.filepath)
# Analyze image for non-zero data section
first_encounter = 0
for col in range(0,1024):
    if values[col] != 0:
        first_encounter = col
        break

bpy.data.cameras["cg.Orthographic_Camera"].lens = 10 * 1024 / (1024 - first_encounter)

#Rerender image
bpy.data.scenes["Scene"].cycles.samples = 512
bpy.context.scene.render.filepath = 'mic.exr'
bpy.ops.render.render(write_still = True)